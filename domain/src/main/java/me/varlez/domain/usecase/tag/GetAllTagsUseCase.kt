package me.varlez.domain.usecase.tag

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import me.varlez.domain.model.Tag
import me.varlez.domain.repository.Repository
import javax.inject.Inject

class GetAllTagsUseCase @Inject constructor(private val tagRepository: Repository<Tag, String>) {

    fun get(): Single<List<Tag>> {
        return tagRepository.getAll()
            .subscribeOn(Schedulers.io())
    }

}