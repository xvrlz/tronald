package me.varlez.domain.usecase.tag

import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers
import me.varlez.domain.model.Tag
import me.varlez.domain.repository.Repository
import javax.inject.Inject

class GetTagUseCase @Inject constructor(private val tagRepository: Repository<Tag, String>) {

    fun get(id: String): Maybe<Tag> {
        return tagRepository.get(id)
            .subscribeOn(Schedulers.io())
    }

}