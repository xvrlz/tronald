package me.varlez.domain.model

data class Tag(
    val title: String,
    val value: String? = null,
    val author: String? = null
)