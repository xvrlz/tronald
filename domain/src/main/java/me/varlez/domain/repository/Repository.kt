package me.varlez.domain.repository

import io.reactivex.Maybe
import io.reactivex.Single

interface Repository<T, ID> {

    fun getAll(): Single<List<T>>
    fun get(id: ID): Maybe<T>

}