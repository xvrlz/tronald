package me.varlez.data

import android.content.Context
import me.varlez.data.di.module.DaggerDataComponent
import me.varlez.data.di.module.DataComponent
import me.varlez.data.di.module.DatabaseModule
import me.varlez.data.di.module.NetworkModule

object DataLayer {

    fun createComponent(context: Context): DataComponent {
        return DaggerDataComponent.builder()
            .databaseModule(DatabaseModule(context))
            .networkModule(NetworkModule())
            .build()
    }

}