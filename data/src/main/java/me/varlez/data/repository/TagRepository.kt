package me.varlez.data.repository

import io.reactivex.Maybe
import io.reactivex.Single
import me.varlez.data.datasource.api.RestApi
import me.varlez.data.datasource.api.mapper.APITagListResponseMapper
import me.varlez.data.datasource.api.mapper.APITagResponseMapper
import me.varlez.data.datasource.database.dao.TagDao
import me.varlez.data.datasource.database.mapper.DBTagMapper
import me.varlez.domain.model.Tag
import me.varlez.domain.repository.Repository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TagRepository @Inject internal constructor(
    private val restApi: RestApi,
    private val tagDao: TagDao
) : Repository<Tag, String> {

    override fun getAll(): Single<List<Tag>> {
        val networkSingle = restApi.getTags()
            .map {
                it.tags.orEmpty()
                    .map(APITagListResponseMapper::map)
            }
            .doOnSuccess {
                val dbEntities = it.map(DBTagMapper::reverseMap)
                tagDao.insertOrUpdate(dbEntities)
            }

        val databaseSingle = tagDao.getAll()
            .map { it.map(DBTagMapper::map) }
            .toSingle()

        return networkSingle.onErrorResumeNext(databaseSingle)
    }

    override fun get(id: String): Maybe<Tag> {
        val networkMaybe = restApi.getTag(id)
            .map(APITagResponseMapper::map)
            .doOnSuccess {
                tagDao.insertOrUpdate(DBTagMapper.reverseMap(it))
            }

        val databaseMaybe = tagDao.get(id)
            .filter { it.isComplete }
            .map(DBTagMapper::map)

        return networkMaybe.onErrorResumeNext(databaseMaybe)
    }

}