package me.varlez.data.common

internal interface Mapper<E, D> {

    fun map(input: E): D

    fun reverseMap(input: D): E {
        throw UnsupportedOperationException("Reverse mapping not implemented")
    }

}