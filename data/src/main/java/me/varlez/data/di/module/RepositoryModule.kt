package me.varlez.data.di.module

import dagger.Binds
import dagger.Module
import me.varlez.data.repository.TagRepository
import me.varlez.domain.model.Tag
import me.varlez.domain.repository.Repository

@Module
internal abstract class RepositoryModule {

    @Binds
    abstract fun bindTagRepository(repository: TagRepository): Repository<Tag, String>
}