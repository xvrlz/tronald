package me.varlez.data.di.module

import dagger.Component
import me.varlez.domain.model.Tag
import me.varlez.domain.repository.Repository
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, DatabaseModule::class, RepositoryModule::class])
interface DataComponent {
    fun tagRepository(): Repository<Tag, String>
}