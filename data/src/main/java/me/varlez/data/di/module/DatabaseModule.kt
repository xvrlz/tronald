package me.varlez.data.di.module

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import me.varlez.data.datasource.database.TronaldDumpDatabase
import me.varlez.data.datasource.database.dao.TagDao
import javax.inject.Singleton

@Module
internal class DatabaseModule(private val appContext: Context) {

    @Provides
    @Singleton
    fun provideDatabase(): TronaldDumpDatabase {
        return Room.databaseBuilder(
            appContext,
            TronaldDumpDatabase::class.java,
            TronaldDumpDatabase.DB_NAME
        )
            .build()
    }

    @Provides
    @Singleton
    fun provideTagDao(database: TronaldDumpDatabase): TagDao {
        return database.tagDao()
    }

}