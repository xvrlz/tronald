package me.varlez.data.datasource.api

import io.reactivex.Maybe
import io.reactivex.Single
import me.varlez.data.datasource.api.entity.APITagResponse
import me.varlez.data.datasource.api.entity.APITagsListResponse
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Tronald Dump REST API interface for Retrofit.
 */
internal interface RestApi {

    // App content

    @GET("tag")
    fun getTags(): Single<APITagsListResponse>

    @GET("tag/{tagName}")
    fun getTag(@Path("tagName") tagName: String): Maybe<APITagResponse>


    companion object {
        const val BASE_URL = "https://api.tronalddump.io/"
    }

}