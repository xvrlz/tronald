package me.varlez.data.datasource.api.entity

import com.google.gson.annotations.SerializedName

internal data class APITagResponse(
    @SerializedName("_embedded")
    val content: APITagList?
) {
    data class APITagList(val tags: List<APITag>? = emptyList())

    data class APITag(
        val tags: List<String>? = emptyList(),
        val value: String? = "",
        @SerializedName("_embedded")
        val metadata: APITagMetadata? = APITagMetadata()
    )

    data class APITagMetadata(
        val author: List<APIAuthor>? = emptyList(),
        val source: List<APISource>? = emptyList()
    )

    data class APIAuthor(
        val name: String? = "(Unknown)"
    )

    data class APISource(
        val url: String?
    )
}