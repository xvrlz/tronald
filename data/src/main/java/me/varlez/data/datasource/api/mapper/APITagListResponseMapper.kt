package me.varlez.data.datasource.api.mapper

import me.varlez.data.common.Mapper
import me.varlez.domain.model.Tag

internal object APITagListResponseMapper : Mapper<String, Tag> {

    override fun map(input: String) = Tag(input)

}