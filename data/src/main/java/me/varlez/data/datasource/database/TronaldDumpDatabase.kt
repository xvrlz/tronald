package me.varlez.data.datasource.database

import androidx.room.Database
import androidx.room.RoomDatabase
import me.varlez.data.datasource.database.dao.TagDao
import me.varlez.data.datasource.database.entity.DBTag

@Database(entities = [DBTag::class], version = 1)
internal abstract class TronaldDumpDatabase : RoomDatabase() {

    abstract fun tagDao(): TagDao

    companion object {
        const val DB_NAME = "tronalddump.db"
    }

}