package me.varlez.data.datasource.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Tag")
internal data class DBTag(
    @PrimaryKey
    val title: String,
    val value: String?,
    val author: String?
) {
    val isComplete
        get() = value != null && author != null
}