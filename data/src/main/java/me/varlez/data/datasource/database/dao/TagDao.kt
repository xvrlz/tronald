package me.varlez.data.datasource.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Maybe
import me.varlez.data.datasource.database.entity.DBTag

@Dao
internal interface TagDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdate(item: DBTag)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdate(items: List<DBTag>)

    @Query("SELECT * FROM Tag WHERE title=:title")
    fun get(title: String): Maybe<DBTag>

    @Query("SELECT * FROM Tag")
    fun getAll(): Maybe<List<DBTag>>

}