package me.varlez.data.datasource.database.mapper

import me.varlez.data.common.Mapper
import me.varlez.data.datasource.database.entity.DBTag
import me.varlez.domain.model.Tag

internal object DBTagMapper : Mapper<DBTag, Tag> {

    override fun map(input: DBTag): Tag = Tag(
        title = input.title,
        value = input.value,
        author = input.author
    )

    override fun reverseMap(input: Tag): DBTag {
        return DBTag(
            title = input.title,
            value = input.value,
            author = input.author
        )
    }

}