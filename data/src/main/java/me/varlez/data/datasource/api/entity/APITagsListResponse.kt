package me.varlez.data.datasource.api.entity

import com.google.gson.annotations.SerializedName

internal data class APITagsListResponse(
    @SerializedName("_embedded")
    val tags: List<String>? = emptyList()
)