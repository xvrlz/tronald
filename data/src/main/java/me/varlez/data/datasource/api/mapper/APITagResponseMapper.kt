package me.varlez.data.datasource.api.mapper

import me.varlez.data.common.Mapper
import me.varlez.data.datasource.api.entity.APITagResponse
import me.varlez.domain.model.Tag

internal object APITagResponseMapper : Mapper<APITagResponse, Tag> {

    override fun map(input: APITagResponse): Tag {
        // The REST API can return several quotes for a tag.
        // To simplify the app and logic, this only handles the first one.

        val firstTag = input.content?.tags?.firstOrNull()
        val author = firstTag?.metadata?.author?.firstOrNull()

        return Tag(
            firstTag?.tags?.firstOrNull() ?: "",
            firstTag?.value,
            author?.name
        )
    }

}