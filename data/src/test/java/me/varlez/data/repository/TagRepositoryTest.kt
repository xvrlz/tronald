package me.varlez.data.repository

import com.nhaarman.mockito_kotlin.*
import io.reactivex.Maybe
import io.reactivex.Single
import junit.framework.TestCase.assertEquals
import me.varlez.data.datasource.api.APIMockData
import me.varlez.data.datasource.api.DBMockData
import me.varlez.data.datasource.api.RestApi
import me.varlez.data.datasource.api.entity.APITagResponse
import me.varlez.data.datasource.api.entity.APITagsListResponse
import me.varlez.data.datasource.database.dao.TagDao
import me.varlez.data.datasource.database.entity.DBTag
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.nio.channels.UnresolvedAddressException

@RunWith(MockitoJUnitRunner::class)
internal class TagRepositoryTest {

    @Mock
    lateinit var tagDao: TagDao

    @Mock
    lateinit var restApi: RestApi

    private lateinit var repository: TagRepository

    @Before
    fun setUp() {
        // Default mocks
        whenever(tagDao.getAll()).thenReturn(Maybe.never())
        whenever(tagDao.get(any())).thenReturn(Maybe.never())

        repository = TagRepository(restApi, tagDao)
    }

    @Test
    fun `getAll returns REST API data`() {
        // Given
        val apiResponse = Single.just(APIMockData.mockTagListResponse())
        whenever(restApi.getTags()).thenReturn(apiResponse)

        // When
        val testObserver = repository.getAll().test()

        // Then
        testObserver.assertComplete()
        assertEquals(1, testObserver.valueCount())

        val tags = testObserver.values().first()
        assertEquals(3, tags.size)

        val tag = tags.first()
        assertEquals("Tag 1 (API)", tag.title)
    }

    @Test
    fun `getAll returns database entries if REST API is unavailable`() {
        // Given
        val apiResponse = Single.error<APITagsListResponse>(UnresolvedAddressException())
        val dbMaybe = Maybe.just(DBMockData.mockTagList())
        whenever(restApi.getTags()).thenReturn(apiResponse)
        whenever(tagDao.getAll()).thenReturn(dbMaybe)

        // When
        val testObserver = repository.getAll().test()

        // Then
        testObserver.assertComplete()
        assertEquals(1, testObserver.valueCount())

        val tags = testObserver.values().first()
        assertEquals(3, tags.size)

        val tag = tags.first()
        assertEquals("Tag 1 (DB)", tag.title)
    }

    @Test
    fun `getAll saves entries in database`() {
        // Given
        val apiResponse = Single.just(APIMockData.mockTagListResponse())
        whenever(restApi.getTags()).thenReturn(apiResponse)

        // When
        repository.getAll().test()

        // Then
        verify(tagDao).insertOrUpdate(any<List<DBTag>>())
    }

    @Test
    fun `get returns REST API data`() {
        // Given
        val apiResponse = Maybe.just(APIMockData.mockTagResponse())
        whenever(restApi.getTag(any())).thenReturn(apiResponse)

        // When
        val testObserver = repository.get("Yeah").test()

        // Then
        testObserver.assertComplete()
        assertEquals(1, testObserver.valueCount())

        val tag = testObserver.values().first()
        assertEquals("TransferWise (API)", tag.title)
    }

    @Test
    fun `get saves entry in database`() {
        // Given
        val apiResponse = Maybe.just(APIMockData.mockTagResponse())
        whenever(restApi.getTag(any())).thenReturn(apiResponse)

        // When
        repository.get("Yeah").test()

        // Then
        verify(tagDao).insertOrUpdate(any<DBTag>())
    }

    @Test
    fun `get returns database entry if REST API is unavailable`() {
        // Given
        val apiResponse = Maybe.error<APITagResponse>(UnresolvedAddressException())
        val dbContent = Maybe.just(DBMockData.mockTag())
        whenever(restApi.getTag(any())).thenReturn(apiResponse)
        whenever(tagDao.get(any())).thenReturn(dbContent)

        // When
        val testObserver = repository.get("Yeah").test()

        // Then
        testObserver.assertComplete()
        assertEquals(1, testObserver.valueCount())

        val tag = testObserver.values().first()
        assertEquals("TransferWise (DB)", tag.title)

        verify(tagDao).get(eq("Yeah"))
        verify(tagDao, times(0)).insertOrUpdate(any<DBTag>())
    }
}