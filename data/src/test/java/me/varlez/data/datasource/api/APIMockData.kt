package me.varlez.data.datasource.api

import me.varlez.data.datasource.api.entity.APITagResponse
import me.varlez.data.datasource.api.entity.APITagsListResponse

internal object APIMockData {

    fun mockTagResponse(): APITagResponse {
        val author = listOf(APITagResponse.APIAuthor("Donald Trump"))
        val apiTag = APITagResponse.APITag(
            listOf("TransferWise (API)"),
            "Donald loves TransferWise",
            APITagResponse.APITagMetadata(author)
        )

        return APITagResponse(APITagResponse.APITagList(listOf(apiTag)))
    }

    fun mockTagListResponse(): APITagsListResponse {
        val tags = listOf(
            "Tag 1 (API)",
            "Tag 2 (API)",
            "Tag 3 (API)"
        )

        return APITagsListResponse(tags)
    }

}