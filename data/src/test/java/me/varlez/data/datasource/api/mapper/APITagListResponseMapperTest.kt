package me.varlez.data.datasource.api.mapper

import junit.framework.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class APITagListResponseMapperTest {

    @Test
    fun `Mapper maps String to Tag`() {
        val tag = APITagListResponseMapper.map("Title")

        assertEquals("Title", tag.title)
    }
}