package me.varlez.data.datasource.api

import me.varlez.data.datasource.database.entity.DBTag

internal object DBMockData {

    fun mockTag(): DBTag {
        return DBTag("TransferWise (DB)", "Donald loves TransferWise", "Donald Trump")
    }

    fun mockTagList() = listOf(
        DBTag("Tag 1 (DB)", "Value 1", "Author name 1"),
        DBTag("Tag 2 (DB)", "Value 2", "Author name 2"),
        DBTag("Tag 3 (DB)", "Value 3", "Author name 3")
    )

}