package me.varlez.data.datasource.api.mapper

import me.varlez.data.datasource.api.APIMockData
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
internal class APITagResponseMapperTest {

    @Test
    fun `Mapper maps API entity to Tag`() {
        val tag = APITagResponseMapper.map(APIMockData.mockTagResponse())

        assertEquals(tag.title, "TransferWise (API)")
        assertEquals(tag.value, "Donald loves TransferWise")
        assertEquals(tag.author, "Donald Trump")
    }

}