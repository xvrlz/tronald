package me.varlez.tronalddump.ui.list

import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import me.varlez.domain.model.Tag
import me.varlez.domain.usecase.tag.GetAllTagsUseCase
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TagListPresenterTest {

    @Mock
    lateinit var useCase: GetAllTagsUseCase

    @Mock
    lateinit var view: TagListContract.View

    private lateinit var presenter: TagListPresenter

    @Before
    fun setUp() {
        presenter = TagListPresenter(useCase)
        presenter.attach(view)
    }

    @Test
    fun onCreate_calls_view_showData() {
        val data = listOf(Tag("One"), Tag("Two"), Tag("Three"))
        whenever(useCase.get()).thenReturn(Single.just(data))

        presenter.onCreate()

        verify(view).showLoading()
        verify(view).showData(eq(data))
    }

    @Test
    fun onCreate_calls_view_showError() {
        whenever(useCase.get()).thenReturn(Single.error(Exception("Boo!")))

        presenter.onCreate()

        verify(view).showLoading()
        verify(view).showError(eq("Boo!"))
    }

    companion object {
        @JvmStatic
        @BeforeClass
        fun setupClass() {
            RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        }

        @JvmStatic
        @AfterClass
        fun tearDown() {
            RxJavaPlugins.reset()
            RxAndroidPlugins.reset()
        }
    }
}