package me.varlez.tronalddump.ui.details

import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Maybe
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import me.varlez.domain.model.Tag
import me.varlez.domain.usecase.tag.GetTagUseCase
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TagDetailPresenterTest {


    @Mock
    lateinit var useCase: GetTagUseCase

    @Mock
    lateinit var view: TagDetailContract.View

    private lateinit var presenter: TagDetailPresenter

    @Before
    fun setUp() {
        presenter = TagDetailPresenter(useCase)
        presenter.attach(view)
    }

    @Test
    fun `onCreate calls view_showData`() {
        val tag = Tag("Tag", "A random quote", "Donald")
        whenever(useCase.get(anyString())).thenReturn(Maybe.just(tag))

        presenter.onCreate("Random")

        verify(view).showLoading()
        verify(view).showData(eq(TagDetailViewModel("«A random quote»", "Donald")))
    }

    @Test
    fun `onCreate calls view_showError`() {
        whenever(useCase.get(anyString())).thenReturn(Maybe.error(Exception("Boo!")))

        presenter.onCreate("Random")

        verify(view).showLoading()
        verify(view).showError(eq("Boo!"))
    }

    @Test
    fun `onCreate calls view_showError when content not found`() {
        whenever(useCase.get(anyString())).thenReturn(Maybe.empty())

        presenter.onCreate("Random")

        verify(view).showLoading()
        verify(view).showError(eq("Content unavailable"))
    }

    companion object {
        @JvmStatic
        @BeforeClass
        fun setupClass() {
            RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        }

        @JvmStatic
        @AfterClass
        fun tearDown() {
            RxJavaPlugins.reset()
            RxAndroidPlugins.reset()
        }
    }
}