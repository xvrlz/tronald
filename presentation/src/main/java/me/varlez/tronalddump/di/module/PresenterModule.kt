package me.varlez.tronalddump.di.module

import dagger.Binds
import dagger.Module
import me.varlez.tronalddump.ui.details.TagDetailContract
import me.varlez.tronalddump.ui.details.TagDetailPresenter
import me.varlez.tronalddump.ui.list.TagListContract
import me.varlez.tronalddump.ui.list.TagListPresenter

@Module
abstract class PresenterModule {

    @Binds
    abstract fun bindTagListPresenter(tagListPresenter: TagListPresenter): TagListContract.Presenter

    @Binds
    abstract fun bindTagDetialPresenter(tagDetailPresenter: TagDetailPresenter): TagDetailContract.Presenter
}