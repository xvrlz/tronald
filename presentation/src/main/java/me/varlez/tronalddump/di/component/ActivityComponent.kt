package me.varlez.tronalddump.di.component

import dagger.Component
import me.varlez.data.di.module.DataComponent
import me.varlez.tronalddump.di.module.PresenterModule
import me.varlez.tronalddump.ui.details.TagDetailActivity
import me.varlez.tronalddump.ui.list.TagListActivity

@ViewScope
@Component(dependencies = [DataComponent::class], modules = [PresenterModule::class])
interface ActivityComponent {
    fun inject(tagListActivity: TagListActivity)
    fun inject(tagListActivity: TagDetailActivity)
}