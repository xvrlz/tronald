package me.varlez.tronalddump.ui.details

import io.reactivex.android.schedulers.AndroidSchedulers
import me.varlez.domain.model.Tag
import me.varlez.domain.usecase.tag.GetTagUseCase
import me.varlez.tronalddump.common.BasePresenter
import javax.inject.Inject

class TagDetailPresenter @Inject constructor(private val getTagUseCase: GetTagUseCase) :
    TagDetailContract.Presenter,
    BasePresenter<TagDetailContract.View>() {

    private var tag: Tag? = null

    override fun onCreate(tagId: String) {
        val subscription = getTagUseCase.get(tagId)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { view?.showLoading() }
            .subscribe({
                tag = it
                showData(it)
            }, {
                view?.showError(it.localizedMessage)
            }, {
                view?.showError("Content unavailable")
            })

        addSubscription(subscription)
    }

    private fun showData(tag: Tag) {
        val viewModel = TagDetailViewModel(
            text = "«${tag.value}»",
            author = tag.author
        )

        view?.showData(viewModel)
    }

}