package me.varlez.tronalddump.ui.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_tag_list.*
import kotlinx.android.synthetic.main.item_tag.view.*
import me.varlez.domain.model.Tag
import me.varlez.tronalddump.R
import me.varlez.tronalddump.common.BaseActivity
import me.varlez.tronalddump.common.view.MarginItemDecoration
import me.varlez.tronalddump.ui.details.TagDetailActivity
import javax.inject.Inject


class TagListActivity : BaseActivity(), TagListContract.View {

    @Inject
    lateinit var presenter: TagListContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_tag_list)

        activityComponent.inject(this)
        presenter.attach(this)
        presenter.onCreate()

        with(recyclerView) {
            val spanCount = resources.getInteger(R.integer.grid_columns)

            layoutManager = GridLayoutManager(
                context,
                spanCount
            )
            addItemDecoration(
                MarginItemDecoration(
                    spanCount,
                    resources.getDimension(R.dimen.grid_items_spacing).toInt(),
                    true
                )
            )
        }
    }

    override fun onDestroy() {
        presenter.detach()
        super.onDestroy()
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
        recyclerView.visibility = View.GONE
    }

    override fun showData(data: List<Tag>) {
        progressBar.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE

        val adapter = TagsAdapter(data)
        recyclerView.adapter = adapter
        subscribeToClicks(adapter)
    }

    override fun showError(message: String) {
        progressBar.visibility = View.GONE
        recyclerView.visibility = View.GONE

        // TODO nicer error message and retry feature
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun subscribeToClicks(adapter: TagsAdapter) {
        addDisposable(
            adapter.clickSubject
                .subscribe(::handleTagClick, ::handleError)
        )
    }

    private fun handleTagClick(tag: Tag) {
        Log.d("TagList", "ITEM CLICKED: ${tag.title}")
        startActivity(TagDetailActivity.getStartIntent(this, tag.title))
    }

    class TagsAdapter(private val tags: List<Tag>) :
        RecyclerView.Adapter<TagsAdapter.ViewHolder>() {

        val clickSubject = PublishSubject.create<Tag>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val view = layoutInflater.inflate(R.layout.item_tag, parent, false)

            return ViewHolder(view)
        }

        override fun getItemCount(): Int = tags.size

        override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
            viewHolder.bind(tags[position])
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            init {
                view.setOnClickListener { clickSubject.onNext(tags[adapterPosition]) }
            }

            fun bind(tag: Tag) {
                itemView.tagTextView.text = tag.title
            }
        }
    }
}