package me.varlez.tronalddump.ui.list

import me.varlez.tronalddump.common.BaseContract
import me.varlez.domain.model.Tag

class TagListContract {

    interface View : BaseContract.View {
        fun showLoading()
        fun showData(data: List<Tag>)
        fun showError(message: String)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun onCreate()
    }

}