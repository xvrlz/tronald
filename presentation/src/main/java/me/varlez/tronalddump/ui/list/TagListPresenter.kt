package me.varlez.tronalddump.ui.list

import io.reactivex.android.schedulers.AndroidSchedulers
import me.varlez.domain.model.Tag
import me.varlez.domain.usecase.tag.GetAllTagsUseCase
import me.varlez.tronalddump.common.BasePresenter
import javax.inject.Inject

class TagListPresenter @Inject constructor(private val getAllTagsUseCase: GetAllTagsUseCase) :
    TagListContract.Presenter,
    BasePresenter<TagListContract.View>() {

    private var tags: List<Tag>? = null

    override fun onCreate() {
        val subscription = getAllTagsUseCase.get()
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { view?.showLoading() }
            .subscribe({
                tags = it
                view?.showData(it)
            }, {
                view?.showError(it.localizedMessage)
            })

        addSubscription(subscription)
    }

}