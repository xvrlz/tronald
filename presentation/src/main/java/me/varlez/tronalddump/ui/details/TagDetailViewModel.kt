package me.varlez.tronalddump.ui.details

data class TagDetailViewModel(
    val text: String,
    val author: String?
)
