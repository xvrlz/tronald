package me.varlez.tronalddump.ui.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_tag_detail.*
import me.varlez.tronalddump.R
import me.varlez.tronalddump.common.BaseActivity
import javax.inject.Inject

private const val ARG_TAG_TITLE = "tagTitle"

class TagDetailActivity : BaseActivity(), TagDetailContract.View {

    @Inject
    lateinit var presenter: TagDetailContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_tag_detail)

        val extras = intent.extras
        val tagTitle = extras?.getString(ARG_TAG_TITLE)

        if (tagTitle == null) {
            Toast.makeText(this, "Error opening detail", Toast.LENGTH_SHORT).show()
            finish()
        } else {
            title = tagTitle

            activityComponent.inject(this)
            presenter.attach(this)
            presenter.onCreate(tagTitle)
        }
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
        quoteTextView.visibility = View.GONE
    }

    override fun showData(data: TagDetailViewModel) {
        progressBar.visibility = View.GONE
        quoteTextView.visibility = View.VISIBLE

        quoteTextView.text = data.text
        authorTextView.text = data.author
    }

    override fun showError(message: String) {
        progressBar.visibility = View.GONE
        quoteTextView.visibility = View.VISIBLE
        quoteTextView.text = message
    }


    companion object {
        fun getStartIntent(context: Context, tagTitle: String): Intent {
            return Intent(context, TagDetailActivity::class.java).apply {
                putExtra(ARG_TAG_TITLE, tagTitle)
            }
        }
    }
}