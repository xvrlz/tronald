package me.varlez.tronalddump.ui.details

import me.varlez.tronalddump.common.BaseContract

class TagDetailContract {

    interface View : BaseContract.View {
        fun showLoading()
        fun showData(data: TagDetailViewModel)
        fun showError(message: String)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun onCreate(tagId: String)
    }

}