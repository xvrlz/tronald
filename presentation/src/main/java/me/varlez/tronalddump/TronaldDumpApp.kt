package me.varlez.tronalddump

import android.app.Application
import me.varlez.data.DataLayer
import me.varlez.data.di.module.DataComponent

class TronaldDumpApp : Application() {

    lateinit var dataComponent: DataComponent

    override fun onCreate() {
        super.onCreate()

        dataComponent = DataLayer.createComponent(applicationContext)
    }
}