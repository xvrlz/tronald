package me.varlez.tronalddump.common

import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import me.varlez.tronalddump.TronaldDumpApp
import me.varlez.tronalddump.di.component.ActivityComponent
import me.varlez.tronalddump.di.component.DaggerActivityComponent

abstract class BaseActivity : AppCompatActivity() {

    private val subscriptions = CompositeDisposable()

    val activityComponent: ActivityComponent by lazy {
        val dataComponent = (application as TronaldDumpApp).dataComponent

        DaggerActivityComponent.builder()
            .dataComponent(dataComponent)
            .build()
    }

    override fun onDestroy() {
        subscriptions.clear()

        super.onDestroy()
    }

    fun addDisposable(disposable: Disposable) {
        subscriptions.add(disposable)
    }

    fun handleError(throwable: Throwable) {
        Log.e("Error", "handleError", throwable)
    }

}