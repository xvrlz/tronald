package me.varlez.tronalddump.common

class BaseContract {

    interface Presenter<in T : View> {
        fun attach(view: T)
        fun detach()
    }

    interface View
}