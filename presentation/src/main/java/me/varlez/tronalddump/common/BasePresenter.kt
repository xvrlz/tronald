package me.varlez.tronalddump.common

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.lang.ref.WeakReference

abstract class BasePresenter<T : BaseContract.View> {

    private val subscriptions = CompositeDisposable()
    private lateinit var _view: WeakReference<T>

    internal val view: T?
        get() = _view.get()

    fun attach(view: T) {
        _view = WeakReference(view)
    }

    fun detach() {
        subscriptions.clear()
        _view.clear()
    }

    internal fun addSubscription(subscription: Disposable) {
        subscriptions.add(subscription)
    }

}